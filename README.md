# slides

A dedicated repo for Quarto-rendered slide decks.

# Syncing

After rendering, sync the _site folder to S3:

```sh
aws s3 sync _site s3://jcarlson-slides-content
```

Then redeploy the Amplify app:

```sh
aws amplify start-deployment --app-id d3vjkc0to4wmpw --branch-name production --source-url s3://jcarlson-slides-content --source-url-type BUCKET_PREFIX
```

# Final Slides

It's nice to have a consistent set of slides at the end for questions and contact info. Here it is:

```md
#

::: {.r-fit-text}
Questions?
:::

## Be in Touch! {.center style="text-align:center"}

:::{.r-fit-text style="text-align:center;"}
[josh@jcarlson.page](mailto:josh@jcarlson.page)

[https://jcarlson.page](https://jcarlson.page)

[@jcarlson@mapstodon.space](https://mapstodon.space/@jcarlson)
:::
```