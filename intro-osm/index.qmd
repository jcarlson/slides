---
title: Intro To OpenStreetMap
subtitle: What is it, and how do you use it?
description: A whirlwind tour of what OpenStreetMap is, how it works, and how to contribute to the project.
presented: ILGISA Annual Conference 2024
presentdate:
  - 202410
categories: 
  - OpenStreetMap
format:
    revealjs:
        theme: [../tokyo-dark-slides.scss]
        transition: slide
        logo: /media/jc_icon.svg
        footer: "[Home](https://jcarlson.page/talks)"
        slide-number: h.v
        show-slide-number: all
preview-links: false
---

# The Concept

## What it is

> OpenStreetMap is a free, editable map of the whole world made by people like you. It was started in 2004 in the UK, out of frustration with the lack of availability of good map data that was free to use.

- Free
- Editable
- Global
- Twenty years old! 🥳

## "Wikipedia of Maps"

Like Wikipedia, OSM:

::: {.incremental}
- Can be edited in almost any way you see fit
- Has a core user base deeply invested in the project
- Experiences occasional vandalism…
- …And swiftly deals with it
- Has endless hair-splitting and meta-discussions
:::

# The Organizations

## OpenStreetMap Foundation

- **Supports** OSM, but does **not** control it
- Non-profit established in 2006
- Does the legal stuff
- Manages the back end

## OSM US

- OSMF chapter in 2020
- Emphasis on education, development
- Supports several apps and larger projects
- Building bridges with US government groups

# The Users

## Data Consumers

Folks who use OSM data in their projects, like:

::: {.incremental}
- Esri Basemaps
- Red Cross
- Amazon Logistics
- Pokémon GO
- Various federal agencies
- Kendall County
:::

:::{.notes}
Way, *way* more than we can list. [Check it out!](https://welcome.openstreetmap.org/about-osm-community/consumers/)

Being a data consumer can *seem* complex, but you can easily use OSM basemaps or something like the QuickOSM QGIS Plugin to throw some features into your map. Boom! You're a data consumer!
:::

## Contributors

- Folks who edit the map!
- There are over *one million* contributors so far
- By the end of this workshop, maybe **you**?

## Other Folks

- OSMF and Local Chapter Staff
- Developers
- Working group members

# The Dataset

## Elements

- Nodes: lat/lon
- Ways: a list of nodes
- Relations: a list of *anything*

<!-- TODO: Picture -->

:::{.notes}
Relations are too complicated to really discuss in an intro workshop, sorry!

To make it clear: ways do *not have geometry*. It only has a list of nodes, and its geometry is constructed from *those*.
:::

## Attributes

- **NO COLUMNS**
- **NO SCHEMA**

Open-ended keys/values

In OSM, we call them "tags".

```json
{
  "amenity": "fast_food",
  "name": "Burger King",
  "addr:housenumber": "123",
  "addr:street": "Main Street"
}
```

:::{.notes}
It's perfectly valid for an object to have *no attributes*, too. If it's just acting as a vertex for a highway, for example, you don't need to tag it.

In general, if the object participates in a larger object (node in a way, way in a relation), tags are optional.
:::

## Inherent Topology

Moving a node will change *every way* that references it!

<!-- TODO: get a picture / GIF -->

:::{.notes}
This can vastly simplify the editing process in certain situations, like contiguous boundaries or road networks.

For those of you who have ever collaborated on something. Say, boundaries in NG911, imagine if there was no such thing as "snapping your points to theirs". Imagine if they were the same literal nodes, and your boundaries were just *always aligned*.

Also, for those of you interested in network analysis, consider: the OpenStreetMap road network is *already a series of connected nodes and edges*. You can go almost directly from OSM data to routable graphs.
:::

## Changesets

::: {.incremental}
- A bounding box
- A list of things you changed
- Exactly what you did to each thing (updated a tag, moved a node)
- Any comments you want to make
- Source specified (optional, but you always should!)
:::

## Changesets: An Example

[Changeset 141807660](https://www.openstreetmap.org/changeset/141807660){.r-fit-text}

# Vandal Stories

:::{.notes}
Before we get to the editing, I do just want to share a few stories of vandalism, because they're kind of interesting, and they demonstrate the robustness of the data model.
:::

## Pokémon GO

There's actually [an academic paper](https://ica-proc.copernicus.org/articles/2/54/2019/ica-proc-2-54-2019.pdf) about this. It wasn't *all* vandalism.

![Charts from the above-linked article](pokemon-charts.png)

:::{.notes}
TO be clear, not *everything* done by Pokémon GO players was vandalism. The game was an onramp to OSM, and they worked to accurately flesh out the map in their areas.

But still, plenty *was*. In the early days of the game, it was not uncommon to find factories, power plants, forests, and more, *all in a middle school playground.* Players were trying to get the rare Pokémon to spawn somewhere more convenient!

Now, the OSM API can be queried in *really* specific ways. When the OSM community saw this happen, it was a simple matter to query the kinds of features players were adding, specifically coming from brand new users, and even spatially searching within a certain distance of schools.
:::

## Saucon

[Saucon Support](https://openstreetmap.org/user/Saucon Support/blocks), [Saucon Technologies](https://openstreetmap.org/user/Saucon Technologies/blocks)

$$
\begin{equation}
  \begin{array}{r}
    & \textsf{Driver Monitoring} \\
    + & \textsf{OSM Speed Limits} \\
    & \hline \textsf{Incentive to Edit}
  \end{array}
\end{equation}
$$

:::{.notes}
This one's pretty recent. There are a surprising number of companies like this that use OSM as the reference data for things like driver monitoring. Like insurance companies using your driving behavior to adjust your rates based on how safe you drive! Part of that is: are you keeping at or under the speed limit?

If you can just raise the speed limit, suddenly you look like you're driving a lot safer.

Hard to say *exactly* why this particular case was happening, but given that they are a player in the driver monitoring space, it seems like something similar could be happening.

Again, it's easy enough to query edits like this, where users are changing the `maxspeed` tag on huge swaths of features.

This one is an example of how the Data Working Group occasionally steps in when the situation warrants it.
:::

# More Resources

- The [Contribute Map Data](https://wiki.openstreetmap.org/wiki/Contribute_map_data) page of the OSM Wiki
- [JOSM](https://josm.openstreetmap.de/) for power users
- [Mobile App](https://wiki.openstreetmap.org/wiki/Software/Mobile) page of the OSM Wiki
- [OpenStreetMap Discussion Forums](https://community.openstreetmap.org)
- [OSM US Slack Space](https://osmus.slack.com)

# Editing the Map
(This is the hands-on part.)

## Create an Account

[Sign Up!](https://www.openstreetmap.org/user/new)

![](new-user.png)

## Click "Edit"

1. Go to [https://openstreetmap.org](https://openstreetmap.org)
2. Find an area you'd like to map
3. Click the "edit" button in the upper left

![](edit-button.png)

## Take the Tour!

:::{.r-fit-text}
Seriously, don't skip it.
:::

## Go Forth and Map the World!

- Don't be afraid to ask questions
- Read the "Good Practice" wiki page
- Pay attention to local / regional conventions


## Be in Touch! {.center style="text-align:center"}

:::{.r-fit-text style="text-align:center;"}
[josh@jcarlson.page](mailto:josh@jcarlson.page)

[https://jcarlson.page](https://jcarlson.page)

[@jcarlson@mapstodon.space](https://mapstodon.space/@jcarlson)
:::